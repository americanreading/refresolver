<?php

namespace AmericanReading\RefResolver\Test;

use AmericanReading\RefResolver\RefResolver;
use PHPUnit_Framework_TestCase;

class RefResolverTest extends PHPUnit_Framework_TestCase
{
    public function testResolveWithNoReferences()
    {
        $expected = <<<JSON
{
    "cat": "Molly",
    "dog": "Bear"
}
JSON;
        $source = <<<JSON
{
    "cat": "Molly",
    "dog": "Bear"
}
JSON;

        $resolver = new RefResolver();
        $obj = json_decode($source);
        $resolver->resolve($obj);
        $this->assertEquals(json_decode($expected), $obj);
    }

    public function testResolveSingleLevelReferences()
    {
        $dir = realpath(__DIR__ . "/../data");
        $source = <<<JSON
{
    "\$ref": "$dir/cats.json",
    "dog": "Bear"
}
JSON;
        $expected = <<<JSON
{
    "cats": [
        {
            "name": "Molly",
            "color": "Calico"
        },
        {
            "name": "Oscar",
            "color": "Orange"
        }
    ],
    "dog": "Bear"
}
JSON;

        $resolver = new RefResolver();
        $obj = json_decode($source);
        $resolver->resolve($obj);
        $this->assertEquals(json_decode($expected), $obj);
    }

    public function testResolveMultiLevelReferences()
    {
        $dir = realpath(__DIR__ . "/../data");
        $resolverFn = function ($ref) use ($dir) {
            $contents = @file_get_contents($ref);
            if ($contents) {
                $contents = str_replace("{{DIRECTORY}}", $dir, $contents);
                return json_decode($contents);
            }
            return null;
        };
        $source = <<<JSON
{
    "\$ref": "$dir/animals.json"
}
JSON;
        $expected = <<<JSON
{
    "animals": {
        "dog": "Bear",
        "cats": [
            {
                "name": "Molly",
                "color": "Calico"
            },
            {
                "name": "Oscar",
                "color": "Orange"
            }
        ]
    }
}
JSON;

        $resolver = new RefResolver($resolverFn);
        $obj = json_decode($source);
        $resolver->resolve($obj);
        $this->assertEquals(json_decode($expected), $obj);
    }

    public function testResolveBadReferences()
    {
        $dir = realpath(__DIR__ . "/../data");
        $source = <<<JSON
{
    "\$ref": "$dir/birds.json",
    "dog": "Bear"
}
JSON;
        $expected = <<<JSON
{
    "\$ref": "$dir/birds.json",
    "dog": "Bear"
}
JSON;

        $resolver = new RefResolver();
        $obj = json_decode($source);
        $resolver->resolve($obj);
        $this->assertEquals(json_decode($expected), $obj);
    }
}

